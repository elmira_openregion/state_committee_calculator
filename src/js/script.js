$( document ).ready(function() {
    
    // открывает контент при выборе определенного раздела(пункта) калькулятора
    $(".js-calc-block" ).on( "click", function() {
    	// при переключение между пунктами калькулятора закрывать все незакрытые таблицы
        $('.calculator-content').removeClass('calculator-content--active');
       	$(".calculator-content-main__card-body").removeClass('calculator-content-main__card-body--active');
	    // -----------------------------------------------------------------------------

        var nameContent = ($(this).attr('data-name'));
        console.log(nameContent);
        if (nameContent){
        	$('.calculator-content[data-atr="'+nameContent+'"]').addClass('calculator-content--active');
        } 
    });

    // разворачивает таблицу при выборе подпункта в разделе
    $(".calculator-content-main__card-header" ).on( "click", function() {
        $(this).siblings().toggleClass('calculator-content-main__card-body--active');
    });

    // отображение стрелки пдля прокрутик к верхней части сайта
    var btn = $("#top_window");
    $(window).scroll(function() {
        if ($(window).scrollTop() > 200) {
          btn.addClass('show');
        } else {
          btn.removeClass('show');
        }
    });


});